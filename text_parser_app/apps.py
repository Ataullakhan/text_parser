from django.apps import AppConfig


class TextParserAppConfig(AppConfig):
    name = 'text_parser_app'
