import boto3
from PIL import Image
from PIL import ImageEnhance
import re
import math
from collections import Counter
from text_parser import settings

path = settings.MEDIA_ROOT


def get_cosine(vec1, vec2):
    intersection = set(vec1.keys()) & set(vec2.keys())
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    if not denominator:
        return 0.0
    else:
        return float(numerator) / denominator


def text_to_vector(text):
    word = re.compile(r'\w+')
    words = word.findall(text)
    return Counter(words)


def get_result(content_a, content_b):
    text1 = content_a
    text2 = content_b
    vector1 = text_to_vector(text1)
    vector2 = text_to_vector(text2)
    cosine_result = get_cosine(vector1, vector2)
    return cosine_result



def adjust_sharpness(input_image, output_image, factor):
    image = Image.open(input_image)
    enhancer_object = ImageEnhance.Sharpness(image)
    out = enhancer_object.enhance(factor)
    out.save(output_image)


black = (0, 0, 0)
white = (255, 255, 255)
threshold = (160, 160, 160)


def textrect(image_file):
    """

    :return:
    """
    text3 = ""
    with open(image_file, 'rb') as document:
        imagebytes = bytearray(document.read())

        # Amazon Textract client
    textract = boto3.client('textract')
    # Call Amazon Textract
    response = textract.detect_document_text(Document={'Bytes': imagebytes})
    for item in response["Blocks"]:
        if item["BlockType"] == "LINE":
            text3 += item["Text"] + ','


    # print(text3)
    return text3


def forms_texract(image_file):
    """

    :param image_file:
    :return:
    """
    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_kv_map(image_file):

        with open(image_file, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['FORMS'])

        # Get the text blocks
        blocks = response['Blocks']

        # get key and value maps
        key_map = {}
        value_map = {}
        block_map = {}
        for block in blocks:
            block_id = block['Id']
            block_map[block_id] = block
            if block['BlockType'] == "KEY_VALUE_SET":
                if 'KEY' in block['EntityTypes']:
                    key_map[block_id] = block
                else:
                    value_map[block_id] = block

        return key_map, value_map, block_map

    def get_kv_relationship(key_map, value_map, block_map):
        kvs = {}
        for block_id, key_block in key_map.items():
            value_block = find_value_block(key_block, value_map)
            key = get_text(key_block, block_map)
            val = get_text(value_block, block_map)
            kvs[key] = val
        return kvs

    def find_value_block(key_block, value_map):
        for relationship in key_block['Relationships']:
            if relationship['Type'] == 'VALUE':
                for value_id in relationship['Ids']:
                    value_block = value_map[value_id]
        return value_block

    def get_text(result, blocks_map):
        text = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
        return text


    def search_value(kvs, search_key):
        for key, value in kvs.items():
            if re.search(search_key, key, re.IGNORECASE):
                return value

    # MAIN PROGRAM
    key_map, value_map, block_map = get_kv_map(image_file)

    # Get Key Value relationship
    kvs = get_kv_relationship(key_map, value_map, block_map)

    return kvs


def table_textract(image_file):
    """

    :return:
    """
    img = Image.open(image_file).convert("LA")

    pixels = img.getdata()

    newPixels = []

    # Compare each pixel
    for pixel in pixels:
        if pixel < threshold:
            newPixels.append(white)
        else:
            newPixels.append(black)

    # Create and save new image.
    newImg = Image.new("RGB", img.size)
    newImg.putdata(newPixels)
    newImg.save("newImage.png")

    adjust_sharpness('newImage.png',
                     'new_sharp_image.png',
                     1.7)

    file_name = 'new_sharp_image.png'

    # get the results
    client = boto3.client(
        service_name='textract',
        region_name='us-east-1',
        endpoint_url='https://textract.us-east-1.amazonaws.com',
    )

    def get_rows_columns_map(table_result, blocks_map):
        rows = {}
        for relationship in table_result['Relationships']:
            if relationship['Type'] == 'CHILD':
                for child_id in relationship['Ids']:
                    cell = blocks_map[child_id]
                    if cell['BlockType'] == 'CELL':
                        row_index = cell['RowIndex']
                        col_index = cell['ColumnIndex']
                        if row_index not in rows:
                            # create new row
                            rows[row_index] = {}

                        # get the text value
                        rows[row_index][col_index] = get_text(cell, blocks_map)
        return rows

    def get_text(result, blocks_map):
        text = ''
        if 'Relationships' in result:
            for relationship in result['Relationships']:
                if relationship['Type'] == 'CHILD':
                    for child_id in relationship['Ids']:
                        word = blocks_map[child_id]
                        if word['BlockType'] == 'WORD':
                            text += word['Text'] + ' '
                            text = text.replace(',', '')

        return text

    def get_table_csv_results(file_name):

        with open(file_name, 'rb') as file:
            img_test = file.read()
            bytes_test = bytearray(img_test)

        # process using image bytes
        response = client.analyze_document(Document={'Bytes': bytes_test}, FeatureTypes=['TABLES'])

        # Get the text blocks
        blocks = response['Blocks']
        # pprint(blocks)

        blocks_map = {}
        table_blocks = []
        for block in blocks:
            blocks_map[block['Id']] = block
            if block['BlockType'] == "TABLE":
                table_blocks.append(block)

        if len(table_blocks) <= 0:
            return "<b> NO Table FOUND </b>"

        csv = ''
        for index, table in enumerate(table_blocks):
            csv += generate_table_csv(table, blocks_map, index + 1)
            csv += '\n\n'
            # csv += generate_table_csv(table, blocks_map, index +1)
        return csv

    def generate_table_csv(table_result, blocks_map, table_index):
        rows = get_rows_columns_map(table_result, blocks_map)

        table_id = 'Table_' + str(table_index)
        # if(table_index > 1):

        # get cells.

        csv = 'Table: {0}\n\n'.format(table_id)

        for row_index, cols in rows.items():

            for col_index, text in cols.items():
                csv += '{}'.format(text) + ","
            csv += '\n'

        csv += '\n\n\n'
        return csv

    table_csv = get_table_csv_results(file_name)
    output_file = 'output.csv'

    # replace content
    with open(path + '/' + output_file, "wt") as fout:
        fout.write(table_csv)

    return table_csv
