from django.shortcuts import render
from text_parser import settings
from django.core.files.storage import FileSystemStorage
from text_parser_app.utils import textrect, forms_texract, table_textract, get_result
import os
import re

path = settings.MEDIA_ROOT


def home(request):
    """
    :param request:
    :return:
    """
    # lookup_list = ['CHICKEN BIRYANI', 'CKM-TENGRI KEBAB', 'FIRNI']

    if os.path.exists(path + "/output.csv"):
        os.remove(path + "/output.csv")
    try:
        if request.method == 'POST' and request.FILES['files']:
            myfile = request.FILES['files']
            fs = FileSystemStorage()
            if os.path.exists(path + '/' + myfile.name):
                os.remove(path + '/' + myfile.name)
                filename = fs.save(myfile.name, myfile)
                files = path + '/' + filename
                text1 = textrect(files)


                # text = text.split(",")
                text = text1.replace(",", " ")
                # Adhar Card Regex
                # regex = re.compile(r'\d{4}\s\d{4}\s\d{4}')
                adhar_card = re.findall(r'\d{4}\s\d{4}\s\d{4}', text)

                # Pan card Regex
                # regex = re.compile(r'[A-Za-z]{5}\d{4}[A-Za-z]{1}')
                pan_card = re.findall(r'[A-Za-z]{5}\d{4}[A-Za-z]{1}', text)

            #   PinCode
                pincode = ""
                client2 = re.finditer(r'(\bPin Code\b\s\d{6})', text)
                for txt in client2:
                    pincode = txt.group()
                    pincode = pincode.strip('Pin Code')
            #  Ragistration Number
                reg_number = ""
                reg_n = re.finditer(r'(\bRegistration No*\b)(.{14})', text)
                for txt in reg_n:
                    reg_number = txt.group()
                    reg_number = reg_number.strip('Registration No.')
            #  Name
                name = ""
                re_name = re.finditer(r'\b(\S+\s){1}\bS/o\b(\s\S+){2}', text)
                for txt in re_name:
                    name = txt.group()
            # Age
                age = ""
                age_re = re.finditer(r'(\baged*\b)(.{10})', text)
                for txt in age_re:
                    age = txt.group()

            else:
                filename = fs.save(myfile.name, myfile)
                files = path + '/' + filename
                text1 = textrect(files)
                text = text1.replace(",", " ")
                # Adhar Card Regex
                # regex = re.compile(r'\d{4}\s\d{4}\s\d{4}')
                adhar_card = re.findall(r'\d{4}\s\d{4}\s\d{4}', text)

                # Pan card Regex
                # regex = re.compile(r'[A-Za-z]{5}\d{4}[A-Za-z]{1}')
                pan_card = re.findall(r'[A-Za-z]{5}\d{4}[A-Za-z]{1}', text)

                #   PinCode
                pincode = ""
                client2 = re.finditer(r'(\bPin Code\b\s\d{6})', text)
                for txt in client2:
                    pincode = txt.group()
                    pincode = pincode.strip('Pin Code')

                #  Ragistration Number
                reg_number = ""
                reg_n = re.finditer(r'(\bRegistration No*\b)(.{14})', text)
                for txt in reg_n:
                    reg_number = txt.group()
                    reg_number = reg_number.strip('Registration No.')
                #  Name
                name = ""
                re_name = re.finditer(r'\b(\S+\s){1}\bS/o\b(\s\S+){2}', text)
                for txt in re_name:
                    name = txt.group()
                # Age
                age = ""
                age_re = re.finditer(r'(\baged*\b)(.{10})', text)
                for txt in age_re:
                    age = txt.group()

                # form_text = forms_texract(files)
                # table_textract(files)

            return render(request, 'home.html', {
                "filename": filename,
                "text": text1,
                "adhar_card": adhar_card,
                "pan_card": pan_card,
                "pincode": pincode,
                "reg_number": reg_number,
                "name": name,
                "age": age




                # "form_text": form_text,
                # "table_texract": table_texract
            })
    except Exception as e:
        print("Exception", e)
    return render(request, 'home.html')
